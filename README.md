DoraDjangoKit
--------------

Contents
---------
- [Pre-requisites](#pre-requisites)
- [Getting Started](#getting-started)
- [Git](#git-best-practices)
- [Renaming Project](#renaming-project)
- [Obtaining API Keys](#getting-api-keys)
- [Project Structure](#project-structure)
- [Contributing](#contributing)


<hr>

Pre-requisites
--------------

For Python-specific libraries, this project relies on [pip](https://pypi.python.org/pypi/pip). The easiest way to install `pip` can be [found here](https://pip.pypa.io/en/latest/installing.html).

Xampp ( Apache, MySQL, Phpmyadmin) [Setting up](http://devendradora.blogspot.in/2015/02/setting-up-xampp.html)

Git  [Download Git bash](http://git-scm.com/downloads) .

If pip install MySQL-python fails , then download from http://www.lfd.uci.edu/~gohlke/pythonlibs/#mysql-python
 $ pip install MySQL_python-1.2.5-cp27-none-win_amd64.whl

<hr>

Getting Started
---------------

To get up and running, simply do the following:
    
    1) Clone the repo

       $ git clone <url>
       $ cd DoraDjangoKit

    2) Create a new copy of DoraDjangoKit/settings.temp.py and rename settings.temp.py to settings.py
   
    3) Create a MYSQL/MariaDB database say DoraDjangoKitdb and update the db details in DoraDjangoKit/settings.py

    4) Install and Create a Virtual environment ( Recommeded , but optional ).If pip install doesn't work behind proxy then 
       $ pip install <packagename> --proxy http://172.30.0.14:3128        
      
       $ pip install virtualenv   (or)  
       $ pip install virtualenv --proxy http://172.30.0.14:3128      
       $ virtualenv venv
        
       
       For Linux :        
       $ source venv/bin/activate
       
       For windows :           
       $ venv\Scripts\activate
     

    5) Install the requirements
       
       $ pip install -r requirements.txt --proxy http://172.30.0.14:3128

    5) Install front end dependencies using bower ( optional )
      
       $ npm install -g bower
       $ bower install

    6) Perform database migrations and runserver

       $ cd DoraDjangoKit
       $ python manage.py makemigrations
       $ python manage.py migrate
       $ python manage.py runserver

     Open your browser and type http://localhost:8000/
     To access admin panel http://localhost:8000/manage

    7) To create a new app (module) inside a project
       
       $ python manage.py startapp <appname>

    8) If you installed any packages through pip and wanted to update requirements.txt

       $ pip freeze > requirements.txt


<hr>

GIT
---
 ### For first time configuration

1) Open Git Bash
    
    $ git config --global user.name "Devendra Dora"
    $ git config --global user.email "dev.tech24@gmail.com"

2) use git with proxy server

   $ git config --global http.proxy http://172.30.0.14:3128
   $ git config --global https.proxy https://172.30.0.14:3128

3) use git without  proxy server
   $ git config --global --unset http.proxy
   $ git config --global --unset https.proxy

4) To work on exising git repo say DoraDjangoKit

   $ git clone https://gitlab.com/devendradora/DoraDjangoKit.git
   $ cd DoraDjangoKit

5) To make DoraDjangoKit in your local system as a git repo and push it to online say gitlab
   - create a new repository on say gitlab and name it as DoraDjangoKit
   - Open git bash 
       $ cd DoraDjangoKit
       $ git init
       $ git remote add origin https://gitlab.com/devendradora/DoraDjangoKit.git
       $ git add .
       $ git commit -m "My first commit"
       $ git push -u origin master

### Simple Guide of Git can be [found here](http://rogerdudler.github.io/git-guide/)

### Git best practices

1) Check all the branches in your project
     
     $ git branch

2) To create a new branch, use git checkout -b <branchname>

    $ git checkout -b develop

3) To switch to any of the available branch say master
   
    $ git checkout master

4) Always prefer to write code in a branch other than master baranch(say develop),so switch your branch to develop

   $ git checkout develop

5) Add / Edit / Delete files in your project

6) To check the status of added/edited/deleted files
   
   $ git status   

7) Add files to index stage
     
     - Some files say file1.txt file2.py file3.cpp          
          $ git add file.txt file2.py file3.cpp

     - To add all files/folders of current directory
          $ git add .  or $ git add --all 

8) you can use gitk tool to see the various files in index stage or commit stage in gui

    $ gitk  or $ git log --graph

9) After adding all the necessary files to the index stage , it's time to commit the changes
   
   $ git commit -m "message describing the changes"
   
10) Always prefer to fetch / pull changes from remote/origin master branch to any other branch(say develop) in local

   $ git pull origin master

11) Make the changes in any file(s) if needed or resolve the merge conflicts if any and then merge the changes to master branch
    
    - If no changes made after git pull
         
         $ git checkout master
         $ git merge develop

    - If changes are made after git pull

       $ git add filename1
       $ git commit -m "message describing changes"
       $ git checkout master
       $ git merge develop

12) To make your changes on local system to be available to all other project members, you have to push changes to remote origin

    $ git push  origin master


13) To change last commit message or adding files in index stage to existing commit
    $ git commit --amend -m "updated commit message"

14) To delete a branch 
    
    $ git branch -d branchname

15) To rename a branch on local

    $ git branch -m old_branch new_branch
    $ git push origin new_branch

16) To remove old branch from remote:

    $ git push origin :old_branch

17) List your existing remote origin url 

     $ git remote -v

18) Change your remote's URL 
    
    $ git remote set-url origin https://github.com/USERNAME/OTHERREPOSITORY.git





<hr>

Renaming Project
----------------

- Rename dora_djangostarter folder ( u will find 3 occurences ) to name of the project (say DoraDjangoKit) you want.
- Navigate to DoraDjangoKit/DoraDjangoKit/manage.py and edit the following line in manage.py to appear as below
  
  os.environ.setdefault("DJANGO_SETTINGS_MODULE", "DoraDjangoKit.settings")

- Navigate to DoraDjangoKit/DoraDjangoKit/DoraDjangoKit/settings.py and edit the following lines in settings.py to appear as below

   ROOT_URLCONF = 'DoraDjangoKit.urls'
   WSGI_APPLICATION = 'DoraDjangoKit.wsgi.application'

- Navigate to DoraDjangoKit/DoraDjangoKit/DoraDjangoKit/wsgi.py and edit the following lines in settings.py to appear as below
 
     os.environ.setdefault("DJANGO_SETTINGS_MODULE", "DoraDjangoKit.settings")



<hr>

Getting API Keys
----------------

### Google plus

1. Register an account on [Google.com](https://accounts.google.com/signup).
2. Navigate to [Google Developer Console](https://console.developers.google.com/project).
3. Click on **Create Project**, give your app a name and click **Create** (this might take a few sceonds).
4. You will be redirected to the project dashboard. From the left menu choose **APIs & auth** and then choose **APIs**.
5. Choose the API you would like to use (the built in example uses **Google+ API**).
6. Click on **Enable API**.
7. From the side menu, under **APIs & auth** select **consent screen**.
    * Fill your app name under **Product Name**.
    * Hit **save** button on the bottom.
8. From the side menu, under **APIs & auth** select credentials:
    * Click on **Create new Client ID**.
    * Under **Authorized JavaScript origins** specify you app base address (e.g ```http://localhost:8000```).
    * Under **Authorized redirect URIs** specify the URL to be redirected after authentication is complete.
    * Hit **Create Client ID** button (this might also take a few seconds).
9. Copy your new generated ```client_id``` and ```client_secret```:
10. Under ```settings.py``` change the following values:
    * ```GOOGLE_PLUS_APP_ID = your_client_id```
    * ```GOOGLE_PLUS_APP_SECRET = your_client_secret```

<hr>

### Facebook

1. Register an account on [Facebook.com](http://www.facebook.com.com/)
2. Visit [Facebook Developers page](https://developers.facebook.com/)
3. After logging in, Click on **My Apps** and then on **Add a New App**
    * Choose W**ebsite** as the platform and add the **name** for your project
    * Give your app a name.
    * Choose the category your app falls into.
    * Click **Create App ID**
    * Skip the quickstart process and you will be redirected to the app dashboard.
4. Copy the **app ID** and the **app secret**.
5. From the left menu choose the **Settings** option.
6. Click on **Add Platform** and choose **Website** once again.
7. Under **site URL**, specift the URL to be redirected after authentication is complete.
8. Click save.
9. In ```settings.py``` change the following values:
    * ```FACEBOOK_APP_ID = your_app_id```
    * ```FACEBOOK_APP_SECRET = your_app_secret```


<hr>

### Github

1. Register an account on [Github.com](http://www.github.com/).
2. Visit [Github developer applications page](https://github.com/settings/developers)
3. Click on **Register new application**.
    * Enter `Application name` and `Homepage URL` field
    * For `redirect url` field, enter: `http://127.0.0.1:8000/land/`
4. Click **Register application**.
5. Within `settings.py`, add the following:
    * `GITHUB_CLIENT_ID` = `Github-client-id`
    * `GITHUB_CLIENT_SECRET` = `Github-client-secret`

<hr>


### Yelp

1. Register an account on [Yelp.com](http://www.yelp.com/)
2. Visit the [Yelp for developers page](https://www.yelp.com/developers/manage_api_keys)
3. You will obtain the following: `CONSUMER KEY`, `CONSUMER SECRET`, `TOKEN`, `TOKEN_SECRET`
4. Within `settings.py`, add the following:
    * `YELP_CONSUMER_KEY` = `Yelp Consumer Key`
    * `YELP_CONSUMER_SECRET` = `Yelp Consumer Secret`
    * `YELP_TOKEN` = `Yelp Token`
    * `YELP_TOKEN_SECRET` = `Yelp Token Secret`

<hr>

### Twitter

1. Register an account on [Twitter.com](http://www.twitter.com/)
2. Visit [Twitter application management page](https://apps.twitter.com/)
3. Click on **Create New App**
    * Enter `Application name`, `Description`, and `Website` field
    * For `Callback URL` field, enter: `http://127.0.0.1:8000/land/`
4. Click **Create your Twitter application**
5. Go to the **Permissions** tab
6. Under *Access*, select **Read and Write** type
7. Go to **Keys and Access Tokens** tab
8. Under *Your Access Token*, click on **Create my access token** to generate access tokens
9. Within `settings.py`, add the following:
    * `TWITTER_CONSUMER_KEY` = `Twitter-consumer-key`
    * `TWITTER_CONSUMER_SECRET` = `Twitter-consumer-secret`
    * `TWITTER_ACCESS_TOKEN` = `Twitter-access-token`
    * `TWITTER_ACCESS_TOKEN_SECRET` = `Twitter-access-token-secret`

<hr>

### Instagram

1. Register an account on [Instagram.com](http://www.instagram.com/).
2. Visit [Instagram manage clients page](https://instagram.com/developer/clients/manage/)
3. Click on **Register a New Client**
    * Enter `Application name`, `Description`, and `Website URL` field
    * For `Redirect URI` field, enter: `http://127.0.0.1:8000/land/`
4. Within `settings.py`, add the following:
    * `INSTAGRAM_CLIENT_ID` = `Instagram-client-id`
    * `INSTAGRAM_CLIENT_SECRET` = `Instagram-client-secret`

<hr>

### Linkedin

1. Register an account on [Linkedin.com](http://www.linkedin.com/).
2. Visit [Linkedin developer Network page](https://www.linkedin.com/secure/developer)
3. Click on **Add New Application**
    * Enter `Company Info`, `Application Info`, and `Contact Info` section
    * Under `OAuth User Agreement` section, select scopes needed
    * For `OAuth 2.0 Redirect URLs` field, enter: `http://127.0.0.1:8000/land/`
4. Click **Add Application**
5. Within `settings.py`, add the following:
    * `LINKEDIN_CLIENT_ID` = `Linkedin-client-id`
    * `LINKEDIN_CLIENT_SECRET` = `Linkedin-client-secret`

<hr>


### Dropbox

1. Register an account on [Dropbox.com](http://www.dropbox.com).
2. Navigate to [Dropbox Developers](https://www.dropbox.com/developers).
3. From the side menu, select **App Console** and click on **Create app**.
4. Configure the app permissions. This example uses the following configuration:
    * App type- Dropbox API app
    * My app needs access to files already on Dropbox.
    * My app needs access to a user's full Dropbox.
    * **Note:** This kind of configuration will require you to submit your app for approval.
5. Give your app a name and click the **Create app button**.
6. You will be redirected to the app console:
    * Under **Redirect URIs** specify the URL to be redirected after authentication is complete (e.g ```http://locahost:8000/home```) and click **add**.
    * Copy your ```App key``` and ```App secret```.
7. Under ```settings.py``` change the following values:
    * ```DROPBOX_APP_ID = your_app_id```
    * ```DROPBOX_APP_SECRET = your_app_secret```
<hr>

### Foursquare

1. Register and account on [Foursquare.com](https://foursquare.com).
2. Navigate to [Foursquare For Developers](https://developer.foursquare.com).
3. From the top menu bar select **My Apps** and you will be redirected to the app dashboard.
4. Hit **Create a New App**:
    * Give your app a name.
    * Under **Download / welcome page url**, specify your app main url (e.g ```http://www.localhost:8000```).
    * Under **Redirect URI**, specify the URL to be redirected after authentication is complete (e.g ```http://locahost:8000/home```) and click **add**.
    * Scroll all the way to the botttom and hit **Save Changes**.
5. From the App page you were redirected to, copy your ```App key``` and ```App secret```.
6. Under ```settings.py``` change to following values:
    * ```FOURSQUARE_APP_ID = your_client_id```
    * ```FOURSQUARE_APP_SECRET = your_app_secret```
<hr>
















