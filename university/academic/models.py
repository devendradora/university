from django.db import models
from django.contrib.auth.models import User

# Create your models here.

COURSETYPE_CHOICES=(('0','THEORY'),('1','LAB'),('2','DEPT ELECTIVE'),('3','OPEN ELECTIVE'))
COURSERATING_CHOICES =(('0','BELOW AVERAGE'),('1','AVERAGE'),('2','ABOVE AVERAGE'),('3','GOOD'),('4','EXCELLENT'))
FACULTYRATING_CHOICES= (('0','BELOW AVERAGE'),('1','AVERAGE'),('2','ABOVE AVERAGE'),('3','GOOD'),('4','EXCELLENT'))


class Course(models.Model):
	name = models.CharField(blank=False,max_length=256)
	code =  models.CharField(blank=False,max_length=32)
	credits= models.IntegerField(blank=False,default=0)
	type  = models.CharField(max_length=1,choices=COURSETYPE_CHOICES,default=0,blank=False)

class Specalization(models.Model):
	desc = models.CharField(blank=False,max_length=16)

class Department(models.Model):
	name = models.CharField(blank=False,max_length=128)
	code = models.CharField(blank=False,max_length=16)

class Semester(models.Model):
	desc = models.CharField(blank=False,max_length=16)

class Section(models.Model):
	desc = models.CharField(blank=False,max_length=16)

class Session(models.Model):
	name = models.CharField(blank=False,max_length=16)

class Structure(models.Model):
	session = models.ForeignKey(Session)
	spec = models.ForeignKey(Specalization)
	dept = models.ForeignKey(Department)
	sem = models.ForeignKey(Semester)
	sec = models.ForeignKey(Section)	

	
class RegisteredStudent(models.Model):
	user = models.ForeignKey(User,related_name='student')
	structure = models.ForeignKey(Structure)
	course = models.ForeignKey(Course)	
	faculty = models.ForeignKey(User,related_name='faculty')

class Attendance(models.Model):
	regstudent = models.ForeignKey(RegisteredStudent)
	date = models.DateField()
	# start_time = models.TimeField()
	# end_time =models.TimeField()
	number_of_hours



class ExamSchedule(models.Model):
	structure = models.ForeignKey(Structure)
	course = models.ForeignKey(Course)
	date = models.DateField()
	start_time = models.TimeField()
	end_time =models.TimeField()

class Grade(models.Model):
	grade = models.CharField(blank=False,max_length=2)

class Result(models.Model):
	regstudent = models.ForeignKey(RegisteredStudent)
	grade = models.ForeignKey(Grade)

class GPA(models.Model):
	user = models.ForeignKey(User)
	structure = models.ForeignKey(Structure)
	sgpa = models.CharField(blank=False,max_length=8)
	cgpa = models.CharField(blank=False,max_length=8)

class Feedback(models.Model):
	regstudent = models.ForeignKey(RegisteredStudent)
	courserating= models.CharField(max_length=1,choices=COURSERATING_CHOICES,default=1,blank=False)
	facultyrating= models.CharField(max_length=1,choices=FACULTYRATING_CHOICES,default=1,blank=False)






















