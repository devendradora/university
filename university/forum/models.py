from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Discussion(models.Model):
	title = models.CharField(blank=False,max_length=1024)
	desc = models.CharField(blank=False,max_length=65536)
	tags = models.CharField(blank=True,max_length=2048)	
	posted_by = models.ForeignKey(User)
	posted_on = models.DateTimeField()


class DiscussionComment(models.Model):
	discuss = models.ForeignKey(Discussion)
	commented_by = models.ForeignKey(User)
	commented_on = models.DateTimeField()
  


class News(models.Model):
	title = models.CharField(blank=False,max_length=1024)
	desc = models.CharField(blank=False,max_length=65536)	
	tags = models.CharField(blank=True,max_length=2048)
	posted_by = models.ForeignKey(User)
	posted_on = models.DateTimeField()



class NewsComment(models.Model):
	news = models.ForeignKey(News)
	commented_by = models.ForeignKey(User)
	commented_on = models.DateTimeField()
  



  



