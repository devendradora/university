from django.db import models
from django.contrib.auth.models import User


# Create your models here.


ATTENDEE_CHOICES=(('0','NOT INTERESTED'),('1','INTERESTED'),('2','ATTENDED'))

class Event(models.Model):
	title = models.CharField(blank=False,max_length=1024)
	desc  = models.CharField(blank=False,max_length=65536)
	venue = models.CharField(blank=False,max_length=1024)
	lat = models.CharField(blank=False,max_length=64)
	lng = models.CharField(blank=False,max_length=64)
	datetime= models.DateTimeField()
	tags = models.CharField(blank=True,max_length=2048)
	posted_by = models.ForeignKey(User)
	posted_on = models.DateTimeField()   


class EventComment(models.Model):
	event = models.ForeignKey(Event)
	commented_by = models.ForeignKey(User)
	commented_on = models.DateTimeField()

class EventAttendee(models.Model):
	event = models.ForeignKey(Event)
	user= models.ForeignKey(User)
	status = models.CharField(max_length=1,choices=ATTENDEE_CHOICES,default=0,blank=True)

