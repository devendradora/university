from django.conf.urls import patterns, include, url
from django.contrib import admin

from ajax_select import urls as ajax_select_urls



urlpatterns = patterns('',
	url(r'^$', include('devauth.urls')),	
    url(r'^devauth/', include('devauth.urls')),   
    url(r'^manage/', include(admin.site.urls)),
    url(r'^dashboard/$','devauth.views.mydashboard', name='dashboard'),   
    # url(r'^openid/(.*)', SessionConsumer()),
    url(r'^profile/$', 'devauth.views.myprofile', name = 'myprofile'),
    url(r'^livesearch/$', 'devauth.views.livesearch', name='livesearch'),
    url(r'^admin/lookups/', include(ajax_select_urls)),
    url(r'^p/(?P<username>\w+)/$','devauth.views.viewprofile',name='viewprofile'),      
    url(r'^api-dev/$',include('rest_framework.urls', namespace='rest_framework')),

)
