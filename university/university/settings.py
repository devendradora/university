"""
Django settings for  project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/
SITE_TITLE='university'
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'keuhh=0*%do-ayvy*m2k=vss*$7)j8q!@u0+d^na7mi2(^!l!d'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (    
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',    
    'bootstrapform',
    # 'django_openid',
    'django_nose',
    'rest_framework',
    'ajax_select',
    'corsheaders', 
    'devauth', 
    'academic',
    'events',
    'recruitment',    
    'forum',   
      
)


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',  
    #'django_openid_consumer.SessionConsumer',
)


AUTHENTICATION_BACKENDS = ['university.backends.EmailOrUsernameModelBackend']

ROOT_URLCONF = 'university.urls'

WSGI_APPLICATION = 'university.wsgi.application'



# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {

 'default': {
        'ENGINE': 'django.db.backends.sqlite3',       
        'NAME': os.path.join(BASE_DIR, 'universitydb.sqllite3'),
    }
    
    # 'default': {
    #     'ENGINE':'django.db.backends.mysql',
    #     'NAME': 'universitydb',
    #     'USER': 'root',
    #     'PASSWORD': 'dev#9347',
    #     'HOST': 'localhost',
    # }

 }

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),    
)

TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]



# Use nose to run all tests
TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'

# Tell nose to measure coverage on the 'foo' and 'bar' apps
NOSE_ARGS = [
    '--with-coverage',
    '--cover-package=devauth/scripts',
]

CORS_ORIGIN_ALLOW_ALL = True

AUTH_PROFILE_MODULE = "account.UserProfile"

LOGIN_URL="/"

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ],
    'PAGE_SIZE': 10
}


# define the lookup channels in use on the site
AJAX_LOOKUP_CHANNELS = {
    'userlookup'  : ('devauth.lookups', 'UserLookup'),      
     #'username'  :{'model': 'auth.user', 'search_field': 'username'}
     #'email'  :{'model': 'auth.user', 'search_field': 'email'}

}

AJAX_SELECT_BOOTSTRAP = True

############
#   KEYS   #
############

FACEBOOK_APP_ID = '1655911937989826'
FACEBOOK_APP_SECRET ='068a89405c09a6b4309af8978ce75fa8'

GOOGLE_PLUS_APP_ID = '123942221400-18ph2ahju7ofd8hb32vqja39v4dn9n1t.apps.googleusercontent.com'
GOOGLE_PLUS_APP_SECRET = 'i-Wb9IvEyr-yYx3Nap2JW6z2'

GOOGLEMAP_API_KEY = 'AIzaSyAQXuO40USNPLJJHJfSwG69BVkUV2BQSTY'


TWITTER_CONSUMER_KEY = 'sTvmcutkPQ7P4NitMtxeyR4Jt'
TWITTER_CONSUMER_SECRET = '489Q36W631BK0jQn7KtCWRzJ79WLrAkYviJmA24w6UHGLWa2nj'
TWITTER_ACCESS_TOKEN = '3226837321-ewyUwHUswg1IDoQaJ0Bz33Z9A3G65WIo1bB4pSx'
TWITTER_ACCESS_TOKEN_SECRET = 'utp3MR71pGt8ZPuB8ZIVV2RKolzg9ovZOU4ai1ihRW4XM'

LINKEDIN_CLIENT_ID = '75qr72jingh2gv'
LINKEDIN_CLIENT_SECRET ='8X8twH6lWAQyVGy4'

GITHUB_CLIENT_ID = '66ac9a546e56d3df1b15'
GITHUB_CLIENT_SECRET = 'aa083646c08f66923ff911496ff75fa4794742ba'

INSTAGRAM_CLIENT_ID = '2daf8c0dd76c446aaaac1b401d410ae6'
INSTAGRAM_CLIENT_SECRET = 'ceacfcb7e16040d98fd47fcd00ec7a97'

YELP_CONSUMER_KEY = 'EXMisJNWez_PuR5pr06hyQ'
YELP_CONSUMER_SECRET = 'VCK-4cDjtQ9Ra4HC5ltClNiJFXs'
YELP_TOKEN = 'AWYVs7Vim7mwYyT1BLJA2xhNTs_vXLYS'
YELP_TOKEN_SECRET = 'Rv4GrlYxYGhxUs14s0VBfk7JLJY'
