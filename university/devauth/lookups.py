from django.db.models import Q
from django.utils.html import escape
from django.contrib.auth.models import User
from ajax_select import LookupChannel


class UserLookup(LookupChannel):

    def check_auth(self, request):
      # if request.user.get_profile() :
            return True
       
    def get_query(self, q, request):
        return User.objects.filter(Q(phone__istartswith=q) | Q(username__icontains=q) | Q(first_name__icontains=q)| Q(last_name__icontains=q) | Q(email__istartswith=q)).order_by('username')

    def get_result(self, obj):
        u""" result is the simple text that is the completion of what the person typed """
        return obj.username

    def format_match(self, obj):
        """ (HTML) formatted item for display in the dropdown """       
        return u"<a href='/p/%s' > %s  <div><i>%s</i></div></a>" % (escape(obj.username),escape(obj.username), escape(obj.email))
        # return self.format_item_display(obj)

    def format_item_display(self, obj):
        """ (HTML) formatted item for displaying item in the selected deck area """
        return u"<a href='/p/%s' > %s </a> <div><i>%s</i></div>" % (escape(obj.username),escape(obj.username), escape(obj.email))


