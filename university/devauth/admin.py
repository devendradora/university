from django.contrib import admin
from devauth.models import Profile, GoogleProfile, FacebookProfile #,TwitterProfile, GithubProfile, LinkedinProfile


# Register your models here.

admin.site.register(Profile)
admin.site.register(GoogleProfile)
admin.site.register(FacebookProfile)

# admin.site.register(GithubProfile)
# admin.site.register(LinkedinProfile)

# class TwitterProfileAdmin(admin.ModelAdmin):
# 	list_display = ('user','twitter_user')
# admin.site.register(TwitterProfile, TwitterProfileAdmin)

