from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms


from ajax_select import make_ajax_field
from ajax_select.fields import AutoCompleteField

class UserSearchForm(forms.Form):

    q = AutoCompleteField('userlookup',required=True,help_text="",label="",
            attrs={'type':'search'})


class UserForm(forms.ModelForm):
    #password = forms.CharField(widget=forms.PasswordInput())
    username = make_ajax_field(User, 'username', 'taglookup', help_text="GoogleProfile url")
    #profile_url = AutoCompleteSelectField('taglookup', required=False, help_text=None)
    class Meta:
        model = User       
        fields = ('username', 'email', 'password')

class SigninForm(forms.ModelForm):
    username = forms.EmailField(widget=forms.TextInput(attrs={'placeholder':'Email or Username'}),label='')
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder':'Password'}), label='')
    
    class Meta:
        model = User
        fields = ('username','password')
	

class SignupForm(forms.ModelForm):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'First name'}),label='')
    last_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Last name'}), label='')
    username = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Username'}),label='')
    email = forms.EmailField(widget=forms.TextInput(attrs={'placeholder':'Email'}),label='')
    #phone_number = forms.RegexField(regex=r'^\+?1?\d{9,15}$',widget=forms.TextInput(attrs={'placeholder':'Phonenumber'}),label='')
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder':'Password'}), label='')
    password2 =  forms.CharField(widget=forms.PasswordInput(attrs={'placeholder':'Re-type Password'}), label='')
  
    class Meta:
        model = User
        fields = ('first_name','last_name','username','email','password')

    def clean_password2(self):
        if self.cleaned_data['password2'] != self.cleaned_data['password']:
            raise forms.ValidationError('passwords do not match!')
        return self.cleaned_data['password2']

    def clean_email(self):
        email = self.cleaned_data['email']
        user = User.objects.filter(email=email)
        if len(user) > 0:
            raise forms.ValidationError('Email has been already taken')
        return self.cleaned_data['email']

    def clean_username(self):
        username = self.cleaned_data['username']
        user = User.objects.filter(username=username)
        if len(user) > 0:
            raise forms.ValidationError('username has been already taken')
        return self.cleaned_data['username']

