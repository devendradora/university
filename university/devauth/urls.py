from django.conf.urls import patterns, url ,include
from devauth import views
from rest_framework.urlpatterns import format_suffix_patterns


# from rest_framework import routers
# router = routers.DefaultRouter()
# router.register(r'users', views.UserViewSet)
# router.register(r'groups', views.GroupViewSet)

urlpatterns = patterns('',
    #url(r'^', include(router.urls)),   
    url(r'^$', views.index, name='index'),
    url(r'^Oauth_render/$', views.Oauth_render, name='basic_home'),
    url(r'^basic_home/$', views.basic_home, name='basic_home'),
    url(r'^register/$', views.register, name='register'),
    url(r'^signup/$', views.user_signup, name='signup'),
    url(r'^login/$', views.user_login, name='login'),
    url(r'^logout/$', views.user_logout, name='logout'),
    url(r'^api/$', views.api_examples, name='api'),   
    url(r'^githubResume/$', views.githubResume, name='githubResume'),
    url(r'^githubUser/$', views.githubUser, name='githubUser'),
    url(r'^githubTopRepositories/$', views.githubTopRepositories, name='githubTopRepositories'),   
    url(r'^linkedin/$', views.linkedin, name='linkedin'),  
    url(r'^instagram/$', views.instagram, name='instagram'),
    url(r'^instagram_login/$', views.instagram_login, name='instagram_login'),
    url(r'^instagramUser/$', views.instagramUser, name='instagramUser'),
    url(r'^instagramMediaByLocation/$', views.instagramMediaByLocation, name='instagramMediaByLocation'),#
    url(r'^instagramUserMedia/$', views.instagramUserMedia, name='instagramUserMedia'),
    url(r'^twitter/$', views.twitter, name='twitter'),
    url(r'^twitterTweets/$', views.twitterTweets, name='twitterTweets'),
    url(r'^twitter_login/$', views.twitter_login, name='twitter_login'),
    url(r'^github_login/$', views.github_login, name='github_login'),
    url(r'^linkedin_login/$', views.linkedin_login, name='linkedin_login'),
    url(r'^facebook_login/$', views.facebook_login, name='facebook_login'),
    url(r'^facebook/$', views.facebook, name='facebook'),
    url(r'^google_login/$', views.google_login, name='google_login'),
    url(r'^google/$', views.googlePlus, name='googlePlus'),
    url(r'^users/', views.UserViewSet.as_view({'get': 'list'})),
    url(r'^users/(?P<userid>\w+)/$', views.UserViewSet.as_view({'get': 'list'})),

)


urlpatterns = format_suffix_patterns(urlpatterns)
