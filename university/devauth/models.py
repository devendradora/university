from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator

def _upload_profile_pic_name(instance, filename):
    ext = filename.split('.')[-1]
    if instance.pk:
        return 'static/uploads/img/avatar/{}_{}.{}'.format(instance.pk,instance.username,ext)

def _upload_cover_pic_name(instance, filename):
    ext = filename.split('.')[-1]
    if instance.pk:
        return 'static/uploads/img/cover/{}_{}.{}'.format(instance.pk,instance.username,ext)
    
reg_num = models.CharField(blank=False,max_length=32)
phone = models.CharField(blank=True,max_length=15)
profile_pic = models.ImageField(blank=True,default="static/uploads/img/avatar/avatar.png",upload_to=_upload_profile_pic_name)
cover_pic =  models.ImageField(blank=True,default="static/uploads/img/cover/cover.jpg",upload_to=_upload_cover_pic_name)
deactivate = models.CharField(blank=False,default=0,max_length=1)

reg_num.contribute_to_class(User,'reg_num')
phone.contribute_to_class(User, 'phone')
profile_pic.contribute_to_class(User,'profile_pic')
cover_pic.contribute_to_class(User,'cover_pic')
deactivate.contribute_to_class(User,'deactivate')


class Profile(models.Model):
    user = models.ForeignKey(User)
    oauth_token = models.CharField(max_length=200)
    oauth_secret = models.CharField(max_length=200)
    

    def __unicode__(self):
        return unicode(self.user)

class FacebookProfile(models.Model):
    user = models.ForeignKey(User)
    fb_user_id = models.CharField(max_length=100)
    time_created = models.DateTimeField(auto_now_add=True)
    profile_url = models.CharField(max_length=50)
    access_token = models.CharField(max_length=100)

class GoogleProfile(models.Model):
    user = models.ForeignKey(User)
    google_user_id = models.CharField(max_length=100)
    time_created = models.DateTimeField(auto_now_add=True)
    access_token = models.CharField(max_length=100)
    profile_url = models.CharField(max_length=100)

    def __unicode__(self):
        return unicode(self.user)
