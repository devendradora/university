from django.db import models
from django.contrib.auth.models import User
from academic.models import GPA,Session,Specalization,Department,Semester,Section

# Create your models here.

COMPANYTYPE_CHOICES=(('0','GOVT'),('1','MNC'),('2','STARTUP'))
JOBCATEGORY_CHOICES=(('0','NORMAL'),('1','DREAM'),('2','SUPER DREAM'))
SELECTED_CHOICES=(('0','REJECTED'),('1','SELECTED'),('2','WAITLISTED'))


def _upload_company_logo(instance, filename):
    ext = filename.split('.')[-1]
    if instance.pk:
        return 'static/uploads/img/avatar/{}_{}.{}'.format(instance.pk,instance.username,ext)

class Company(models.Model):
	name =  models.CharField(blank=False,max_length=512)
	desc =  models.CharField(blank=False,max_length=65536)
	logo =  models.ImageField(blank=True,default="static/uploads/img/company/default.png",upload_to=_upload_company_logo)
	website =models.CharField(blank=True,max_length=1024)
	type  = models.CharField(max_length=1,choices=COMPANYTYPE_CHOICES,default=0,blank=True)

class JobPosting(models.Model):
	company = models.ForeignKey(Company)
	session = models.ForeignKey(Session)
	spec = models.ForeignKey(Specalization)
	dept = models.ForeignKey(Department)
	sem = models.ForeignKey(Semester)	
	role =   models.CharField(blank=False,max_length=256)
	desc = models.CharField(blank=False,max_length=65536)
	category=models.CharField(max_length=1,choices=JOBCATEGORY_CHOICES,default=0,blank=True) 	
	skills_needed = models.CharField(blank=True,max_length=2048)
	selection_criteria = models.CharField(blank=False,max_length=2048)
	package = models.CharField(blank=False,max_length=64)	
	apply_deadline = models.DateTimeField()
	posted_on =  models.DateTimeField()
	posted_by = models.ForeignKey(User)

class AppliedStudent(models.Model):
	jobposting = models.ForeignKey(JobPosting)
	user = models.ForeignKey(User)
	applied_on =  models.DateTimeField()
	selected = models.CharField(max_length=1,choices=SELECTED_CHOICES,default=0,blank=True)

class InterviewExperience(models.Model):
	appliedstudent = models.ForeignKey(AppliedStudent)
	experience = models.CharField(blank=False,max_length=65536)
	posted_on =  models.DateTimeField()






    
